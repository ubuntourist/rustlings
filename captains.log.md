## 2021.04.11

A fresh cloning.

```
./install.sh
Let's get you set up with Rustlings!
Checking requirements...
SUCCESS: Git is installed
SUCCESS: Rust is installed
SUCCESS: Cargo is installed
SUCCESS: Rust is up to date
Cloning Rustlings at rustlings/...
Checking out version tags/4.3.0...
Installing the 'rustlings' executable...
  Installing rustlings v4.3.0 (/home/kjcole/MEGA/Learn/Rust/rustlings/rustlings)
    Updating crates.io index
 Downloading crates ...
  Downloaded console v0.7.7
  Downloaded indicatif v0.10.3
  Downloaded instant v0.1.9
  Downloaded toml v0.4.10
  Downloaded number_prefix v0.2.8
  Downloaded inotify-sys v0.1.5
  Downloaded clicolors-control v1.0.1
  Downloaded console v0.14.1
  Downloaded quote v1.0.9
  Downloaded scopeguard v1.1.0
  Downloaded serde v1.0.125
  Downloaded unicode-xid v0.2.1
  Downloaded textwrap v0.11.0
  Downloaded autocfg v1.0.1
  Downloaded atty v0.2.14
  Downloaded smallvec v1.6.1
  Downloaded ansi_term v0.11.0
  Downloaded cfg-if v0.1.10
  Downloaded log v0.4.14
  Downloaded syn v1.0.69
  Downloaded unicode-width v0.1.8
  Downloaded proc-macro2 v1.0.26
  Downloaded memchr v2.3.4
  Downloaded terminal_size v0.1.16
  Downloaded vec_map v0.8.2
  Downloaded parking_lot v0.11.1
  Downloaded lazy_static v1.4.0
  Downloaded iovec v0.1.4
  Downloaded slab v0.4.2
  Downloaded cfg-if v1.0.0
  Downloaded bitflags v1.2.1
  Downloaded filetime v0.2.14
  Downloaded aho-corasick v0.7.15
  Downloaded clap v2.33.3
  Downloaded regex v1.4.5
  Downloaded notify v4.0.15
  Downloaded walkdir v2.3.2
  Downloaded termios v0.3.3
  Downloaded lazycell v1.3.0
  Downloaded num-traits v0.2.14
  Downloaded serde_derive v1.0.125
  Downloaded strsim v0.8.0
  Downloaded same-file v1.0.6
  Downloaded parking_lot_core v0.8.3
  Downloaded lock_api v0.4.3
  Downloaded mio-extras v2.0.6
  Downloaded net2 v0.2.37
  Downloaded regex-syntax v0.6.23
  Downloaded libc v0.2.93
  Downloaded mio v0.6.23
  Downloaded inotify v0.7.1
   Compiling libc v0.2.93
   Compiling cfg-if v1.0.0
   Compiling memchr v2.3.4
   Compiling proc-macro2 v1.0.26
   Compiling unicode-xid v0.2.1
   Compiling syn v1.0.69
   Compiling bitflags v1.2.1
   Compiling log v0.4.14
   Compiling unicode-width v0.1.8
   Compiling autocfg v1.0.1
   Compiling cfg-if v0.1.10
   Compiling smallvec v1.6.1
   Compiling scopeguard v1.1.0
   Compiling serde_derive v1.0.125
   Compiling lazy_static v1.4.0
   Compiling regex-syntax v0.6.23
   Compiling serde v1.0.125
   Compiling slab v0.4.2
   Compiling same-file v1.0.6
   Compiling lazycell v1.3.0
   Compiling strsim v0.8.0
   Compiling ansi_term v0.11.0
   Compiling vec_map v0.8.2
   Compiling instant v0.1.9
   Compiling textwrap v0.11.0
   Compiling lock_api v0.4.3
   Compiling walkdir v2.3.2
   Compiling num-traits v0.2.14
   Compiling aho-corasick v0.7.15
   Compiling quote v1.0.9
   Compiling iovec v0.1.4
   Compiling net2 v0.2.37
   Compiling parking_lot_core v0.8.3
   Compiling terminal_size v0.1.16
   Compiling atty v0.2.14
   Compiling inotify-sys v0.1.5
   Compiling filetime v0.2.14
   Compiling termios v0.3.3
   Compiling clicolors-control v1.0.1
   Compiling mio v0.6.23
   Compiling parking_lot v0.11.1
   Compiling clap v2.33.3
   Compiling inotify v0.7.1
   Compiling regex v1.4.5
   Compiling number_prefix v0.2.8
   Compiling mio-extras v2.0.6
   Compiling console v0.14.1
   Compiling console v0.7.7
   Compiling notify v4.0.15
   Compiling indicatif v0.10.3
   Compiling toml v0.4.10
   Compiling rustlings v4.3.0 (/home/kjcole/MEGA/Learn/Rust/rustlings/rustlings)
    Finished release [optimized] target(s) in 1m 48s
  Installing /home/kjcole/.cargo/bin/rustlings
   Installed package `rustlings v4.3.0 (/home/kjcole/MEGA/Learn/Rust/rustlings/rustlings)` (executable `rustlings`)
All done! Run 'rustlings' to get started.
```

Oops. Guess I should have read the README.md. Who'd've thunk the repository's
`install.sh` would clone the repository recursively. Boo.

```
$ cd ~/gits/Rust
$ rm -rf rustlings
$ curl -L https://git.io/install-rustlings | bash -s rustlings/
$ rustlings

       welcome to...
                 _   _ _
  _ __ _   _ ___| |_| (_)_ __   __ _ ___
 | '__| | | / __| __| | | '_ \ / _` / __|
 | |  | |_| \__ \ |_| | | | | | (_| \__ \
 |_|   \__,_|___/\__|_|_|_| |_|\__, |___/
                               |___/

/home/kjcole/.cargo/bin/rustlings must be run from the rustlings directory
Try `cd rustlings/`!

$ cd rustlings
$ rustlings

...

Thanks for installing Rustlings!

Is this your first time? Don't worry, Rustlings was made for
beginners! We are going to teach you a lot of things about Rust, but
before we can get started, here's a couple of notes about how
Rustlings operates:

1. The central concept behind Rustlings is that you solve exercises.
   These exercises usually have some sort of syntax error in them,
   which will cause them to fail compliation or testing. Sometimes
   there's a logic error instead of a syntax error. No matter what
   error, it's your job to find it and fix it!  You'll know when you
   fixed it because then, the exercise will compile and Rustlings will
   be able to move on to the next exercise.
2. If you run Rustlings in watch mode (which we recommend), it'll
   automatically start with the first exercise. Don't get confused by
   an error message popping up as soon as you run Rustlings! This is
   part of the exercise that you're supposed to solve, so open the
   exercise file in an editor and start your detective work!
3. If you're stuck on an exercise, there is a helpful hint you can
   view by typing 'hint' (in watch mode), or running `rustlings hint
   myexercise`.
4. If an exercise doesn't make sense to you, feel free to open an
   issue on GitHub! (https://github.com/rust-lang/rustlings/issues/new).
   We look at every issue, and sometimes, other learners do too so you
   can help each other out!

Got all that? Great! To get started, run `rustlings watch` in order to
get the first exercise. Make sure to have your editor open!
```

----
